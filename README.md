# Arduino Outboard Noise Reduction

This is a (mostly) software implementation of Dolby B noise reduction.

It uses a PJRC Teensy 3.2 and Audio Shield.

It interpolates between different cascaded biquad filters depending on the overall signal strength.

The coefficients were calculated using my online [Cascaded Biquad Calculator](https://wyatt-software.com/biquad/) tool.