#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

// Dolby B encoding and decoding constants and coefficients for biquad filters.
#include "levels.h"

// Connected between a digital IO pin and ground (configured with an internal pull-up resister).
// switch open (HIGH) = noise reduction off. switch closed (LOW) = noise reduction on.
const int enabled_pin = 2;

// Connected between a digital IO pin and ground (configured with an internal pull-up resister).
// switch open (HIGH) = encode. switch closed (LOW) = decode.
const int process_select_pin = 3;

// Select the input pin for the noise reduction level potentiometer.
const int input_level_pin = A6; // (pin 20 on a Teensy 3.2)

// The maximum input voltage is 5V, and the analog input readings are returned as 10-bit integer values.
// The maximum numerical value for an (unsigned) 10-bit number is 1023.
const int input_level_raw_max = 1023;

// Min potentiometer reading before adjustning the input level gain.
// 0 = set every loop gain.
// 1 = set gain if there's any change.
// > 1 set gain only if there's a bigger change.
const int input_level_threshold = 8;

const double input_level_amp_min = 0.0; // Off
const double input_level_amp_max = 2.0; // amplified x2 (1.0 == pass-thru unchanged).

// GUItool: begin automatically generated code
AudioInputI2S            i2sInput;           //xy=129,162
AudioAmplifier           ampLeft;           //xy=351,144
AudioAmplifier           ampRight;           //xy=350,184
AudioAnalyzeRMS          rmsLeft;           //xy=608,83
AudioAnalyzeRMS          rmsRight;           //xy=612,124
AudioFilterBiquad        biquadLeft;        //xy=610,191
AudioFilterBiquad        biquadRight;        //xy=615,233
AudioOutputI2S           i2sOutput;           //xy=834,210
AudioConnection          patchCord1(i2sInput, 0, ampLeft, 0);
AudioConnection          patchCord2(i2sInput, 1, ampRight, 0);
AudioConnection          patchCord3(ampRight, rmsRight);
AudioConnection          patchCord4(ampRight, biquadRight);
AudioConnection          patchCord5(ampLeft, rmsLeft);
AudioConnection          patchCord6(ampLeft, biquadLeft);
AudioConnection          patchCord7(biquadLeft, 0, i2sOutput, 0);
AudioConnection          patchCord8(biquadRight, 0, i2sOutput, 1);
AudioControlSGTL5000     sgtl5000;     //xy=132,77
// GUItool: end automatically generated code

// Adjust the sensitivity of the line-level inputs. Fifteen settings are possible:
// See https://www.pjrc.com/teensy/gui/?info=AudioControlSGTL5000
//
//  0: 3.12 Volts p-p
//  1: 2.63 Volts p-p
//  2: 2.22 Volts p-p
//  3: 1.87 Volts p-p
//  4: 1.58 Volts p-p
//  5: 1.33 Volts p-p  (default)
//  6: 1.11 Volts p-p
//  7: 0.94 Volts p-p
//  8: 0.79 Volts p-p
//  9: 0.67 Volts p-p
// 10: 0.56 Volts p-p
// 11: 0.48 Volts p-p
// 12: 0.40 Volts p-p
// 13: 0.34 Volts p-p
// 14: 0.29 Volts p-p
// 15: 0.24 Volts p-p
const int line_in_level = 5;

// Global variables.
bool enabled = 0;
int process = 0;
int inputLevel = -1000; // There's no chance of ever getting this initial reading.

void setup()
{
  pinMode(enabled_pin, INPUT_PULLUP);
  pinMode(process_select_pin, INPUT_PULLUP);

  Serial.begin(9600);

  AudioMemory(8);
  sgtl5000.enable();
  sgtl5000.lineInLevel(line_in_level);
  sgtl5000.inputSelect(AUDIO_INPUT_LINEIN);
  sgtl5000.volume(0.5); // Only effects the headphone output. Line levels are not changed.
}

void loop()
{
  // TODO: Set coefficients to just pass-thru when disabled.
  enabled = digitalRead(enabled_pin) == LOW;

  // 0 = encode. 1 = decode.
  process = digitalRead(process_select_pin) == LOW;
  
  int reading = analogRead(input_level_pin);
  if (abs(inputLevel - reading) >= input_level_threshold)
  {
    inputLevel = reading;
    double gain = map((double)inputLevel, 0.0, (double)input_level_raw_max, input_level_amp_min, input_level_amp_max);
    ampLeft.gain(gain);
    ampRight.gain(gain);

    Serial.println(reading);
  }
  
  processNoiseReduction(&rmsLeft, &biquadLeft);
  processNoiseReduction(&rmsRight, &biquadRight);
}

// Handles noise reduction for a single channel (i.e.: left or right).
void processNoiseReduction(AudioAnalyzeRMS * rms, AudioFilterBiquad * biquad)
{
  if (!rms->available()) {
    return;
  }

  double signalStrength = rms->read();
  double proportionalIndex = signalStrength * (levels::level_count - 1);
  int lowerIndex = floor(proportionalIndex);
  int upperIndex = ceil(proportionalIndex);
  const double (*pLowerCoefficients)[levels::stage_count][levels::coefficient_count] = &(levels::coefficients[process][lowerIndex]);
  const double (*pUpperCoefficients)[levels::stage_count][levels::coefficient_count] = &(levels::coefficients[process][upperIndex]);
  double mix = fractionalPart(proportionalIndex);
  
  for (int stage = 0; stage < levels::stage_count; stage++)
  {
    double interpolatedCoefficients[levels::coefficient_count];
    for (int i = 0; i < levels::coefficient_count; i++)
    {
      const double lowerCoefficient = *(pLowerCoefficients)[stage][i];
      const double upperCoefficient = *(pUpperCoefficients)[stage][i];

      // Interpolate.
      interpolatedCoefficients[i] = lowerCoefficient + (upperCoefficient - lowerCoefficient) * mix;
    }
    
    biquad->setCoefficients(stage, interpolatedCoefficients);
  }
}

// Gets the fractional part of a number.
static double fractionalPart(double value) {
  double n = abs(value);
  return n - floor(n);
}
